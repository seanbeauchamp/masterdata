from django.contrib import admin
from.models import Person, Department, JobTitle, Gender, Company, EmpStatus, OfficeLocation, Group, TitleRole, NRR

# Register your models here.
admin.site.register(Person)
admin.site.register(Department)
admin.site.register(JobTitle)
admin.site.register(Gender)
admin.site.register(Company)
admin.site.register(EmpStatus)
admin.site.register(OfficeLocation)
admin.site.register(Group)
admin.site.register(TitleRole)
admin.site.register(NRR)