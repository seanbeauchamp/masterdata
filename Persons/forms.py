from django import forms
from .models import Person, MemberRequirements

class NewHireForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ['FirstName', 'LastName', 'Credentials', 'MemberNum', 'AccountName', 'NRR',
                  'StartDate', 'CompanyID', 'JobTitleID', 'DeptID', 'Title',
                  'ManagerID', 'EmpStatus', 'EmpType', 'OfficeLocation', 'OfficeNumber']
        widgets = {
            'StartDate': forms.SelectDateWidget(),
        }

    def clean(self):
        cleaned_data = super(NewHireForm, self).clean()
        JobTitleId = cleaned_data.get("JobTitleID")
        Title = cleaned_data.get("Title")
        if not JobTitleId and not Title:
            raise forms.ValidationError("Please enter an applicable title.")


class NewHireGroupsForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ["CoreGroup"]

    #used to make coregroup field required here without modifying the actual model
    def __init__(self, *args, **kwargs):
        super(NewHireGroupsForm, self).__init__(*args, **kwargs)
        #might need to specify here if we start using this form page for more
        for key in self.fields:
            self.fields[key].required = True


class NewHireMemberReqsForm(forms.ModelForm):

    class Meta:
        model = MemberRequirements
        fields = '__all__'


class LoginForm(forms.Form):
    username = forms.CharField(label="username")
    password = forms.CharField(label="password", widget=forms.PasswordInput())
