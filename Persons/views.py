from django.shortcuts import render, get_object_or_404
from Persons.models import Person, TitleRole, OfficeLocation, JobTitle, Copier, CopierPerson
from .forms import NewHireForm, LoginForm, NewHireGroupsForm, NewHireMemberReqsForm
# imports for the rest API
from rest_framework import viewsets
from serializers import PersonSerializer, UserSerializer, GroupSerializer
# imports needed for login authentication
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.http import *
from django.contrib import messages
# settings imported for the ldap connection
from LDAPauthentication import validateuser, getcurrentuser
from django.conf import settings
from formtools.wizard.views import SessionWizardView
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags
import os
import subprocess


# Create your views here.
class PersonSerialViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer


class UserSerialViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupSerialViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


FORMS = [
    ("main", NewHireForm),
    ("groups", NewHireGroupsForm),
    ("requirements", NewHireMemberReqsForm)
]

TEMPLATES = {"main": "Persons/wizardnewhire.html",
             "groups": "Persons/wizardgroups.html",
             "requirements": "Persons/wizardmemberreqs.html"}

def isCoreGroupNeeded(wizard):
    # only promt to choose a core group if a custom title is entered
    cleaned_data = wizard.get_cleaned_data_for_step("main") or {"Title": ""}
    # return true if the title text-field is empty
    return cleaned_data['Title'] != ""


class NewHireWizard(SessionWizardView):

    def get_template_names(self):
        return [TEMPLATES[self.steps.current]]

    def done(self, form_list, form_dict, **kwargs):
        # modify once we're sure the wizard works and can move on to processing POST data
        #try:
        hire = form_dict['main'].save(commit=False)
        hire.author = self.request.user
            # only pull the core group from this page if the user actually used it
        if "groups" in form_dict:
            hire.CoreGroup = form_dict['groups'].cleaned_data['CoreGroup']
        else:
            hire.CoreGroup = getCoreGroup(self.request, hire.OfficeLocation, hire.JobTitleID)
        hireReqs = form_dict['requirements'].save(commit=False)
        hire.MemberRequirements = hireReqs.MRID
        newUserEmail(hire, hireReqs)
        hire.save()
        hireReqs.save()
        #except:
            #messages.error(self.request, "Something went wrong with the user creation process")

        return redirect('list')


def login_user(request):
    logout(request)
    context = {}
    if request.POST:
        loginform = LoginForm(request.POST)
        if loginform.is_valid():
            user = authenticate(username=loginform.cleaned_data['username'],
                                password=loginform.cleaned_data['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/persons/')
                else:
                    messages.error(request, 'Invalid Login Credentials. Please try again.')
            else:
                messages.error(request, 'Invalid Login Credentials. Please try again.')
    else:
        # refresh the form, otherwise you'll run into expired csrf token issues
        loginform = LoginForm()
    context.update({"form": loginform})
    # context.update({"currentuser": getcurrentuser(request)})
    return render(request, 'Persons/login.html', context)
    # passing context dicts to a form with a csrf_token is a pain, so use the message framework instead
    # for things like login form validation messages


def main(request):
    context = {
        'title': "Master Data",
        'description': "Welcome, " + request.user.username.split("\\")[-1],
        'currentuser': getcurrentuser(request),
    }
    return render(request, 'Persons/main.html', context)


def list(request):
    people_list = Person.objects.all()
    context = {
        'people_list': people_list,
        'title': "People List",
        'description': "This list contains any users currently created via the web UI",
        'currentuser': getcurrentuser(request),
    }
    return render(request, 'Persons/index.html', context)


# @login_required(login_url='/login/')
def newperson(request):
    if not validateuser(request):
        messages.error(request, 'Insufficient login credentials for this request')
        return redirect('login')
    if request.method == "POST":
        form = NewHireForm(request.POST)
        if form.is_valid():
            hire = form.save(commit=False)
            hire.author = request.user
            hire.CoreGroup = getCoreGroup(request, hire.OfficeLocation, hire.JobTitleID)
            setCopiers(request, hire.PID, hire.OfficeLocation, hire.OfficeNumber)
            newUserEmail(hire)
            hire.save()
            return redirect('list')
        #else:
            # messages.error(request, 'Validation Error. Please try again.')
    else:
        form = NewHireForm()
    context = {
        'form': form,
        'currentuser': getcurrentuser(request)
    }
    return render(request, 'Persons/newperson.html', {'currentuser': getcurrentuser(request)})
    # change dictionary back to "context" if test fails

# @login_required(login_url='/login/')
def editperson(request, pk):
    if not validateuser(request):
        messages.error(request, 'Insufficient login credentials for this request')
        return redirect('login')
    post = get_object_or_404(Person, pk=pk)
    if request.method == "POST":
        form = NewHireForm(request.POST, instance=post)
        if form.is_valid:
            hire = form.save(commit=False)
            hire.author = request.user
            #setCopiers(request, hire, hire.OfficeLocation, hire.OfficeNumber) #temporary while working out bugs
            #TO-DO Here: Send your default template email, then start using variable rendering to change values
            html_content = render_to_string('Persons/emailtemplate.html')
            text_content = strip_tags(html_content)

            msg = EmailMultiAlternatives('New User Information', text_content, "test@test.com", ["sean.beauchamp@mcinnescooper.com"])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            hire.save()
            # enableMailbox() <- Issues connecting to Exchange on Breakbox. Focus on this later
            return redirect('list')
    else:
        form = NewHireForm(instance=post)
    context = {
        'form': form,
        'currentuser': getcurrentuser(request)
    }
    return render(request, 'Persons/newperson.html', context)


def enableMailbox():
    psfile = os.path.join(settings.BASE_DIR, 'enablemailbox.ps1')
    p = subprocess.Popen(["powershell.exe", psfile])

def getCoreGroup(request, location, title):
        #Need to get PK of the location and title based on passed values
        userTitle = JobTitle.objects.filter(JobTitle=title)
        userLocation = OfficeLocation.objects.filter(OfficeLocation=location)
        TitleRoleEntry = TitleRole.objects.get(LocationID=userLocation, TitleID=userTitle).GroupID
        coreGroup = TitleRoleEntry
        if coreGroup:
            return coreGroup
        messages.error(request, str(location) + str(title))

def setCopiers(request, user, location, floor):
    locationID = OfficeLocation.objects.get(OfficeLocation=location).OfficeLocationID
    copierFloor = floor[:1]
    if locationID == 1 and int(floor[:1]) != 9: #1 being the ID for HFX. 9th floor takes different number of digits
        copierFloor = floor[:2]
        applicableCopiers = Copier.objects.filter(CopierLocation=locationID, CopierFloor=int(copierFloor))
    elif locationID == 1 and int(floor[:1]) == 9:
        copierFloor = floor[:1]
        applicableCopiers = Copier.objects.filter(CopierLocation=locationID, CopierFloor=int(copierFloor))
    else:
        applicableCopiers = Copier.objects.filter(CopierLocation=locationID)
    for copier in applicableCopiers:
        personcopier = CopierPerson(CopierID=copier, PersonID=user)
        personcopier.save()
    messages.error(request, str(locationID) + str(copierFloor))

def newUserEmail(hire, memReqs):

    securityPass = "x" if memReqs.SecurityPass == True else " "
    context = {
        'Name': hire.FirstName + ' ' + hire.LastName,
        'NRR': hire.NRR,
        'StartDate': hire.StartDate,
        'MemberNum': hire.MemberNum,
        'Title': hire.JobTitleID,
        'Department': hire.DeptID,
        'Manager': hire.ManagerID,
        'EmpStatus': hire.EmpStatus,
        'EmpType': hire.EmpType,
        'Location': hire.OfficeLocation,
        'OfficeNum': hire.OfficeNumber,
        'SecurityPass' : "x" if memReqs.SecurityPass == True else " ",
        'ParkingPass': "x" if memReqs.ParkingPass == True else " ",
        'NameOnDoor': "x" if memReqs.NameOnDoor == True else " ",
        'StandardSupplies': "x" if memReqs.StandardSupplies == True else " ",
        'BusinessCards': "x" if memReqs.BusinessCards == True else " ",
        'SetUpLaptop': "x" if memReqs.SetUpComputer == "Laptop" else " ",
        'SetUpDesktop': "x" if memReqs.SetUpComputer == "Desktop" else " ",
        'TrainingHeadset': "x" if memReqs.TrainingHeadset == True else " ",
        'PolycomPhone': "x" if memReqs.PolycomPhone == True else " ",
        'DeskPhone': "x" if memReqs.DeskPhoneNumber == True else " ",
        'GiveTimekeeper': "x" if memReqs.TimekeeperNumber == True else " ",
        'EliteAccess': "x" if memReqs.EliteAccess == True else " ",
        'OtherSoftware': memReqs.OtherSoftware,
        'RemoteAccess': "x" if memReqs.RemoteAccess == True else " ",
        'MCMobile': "x" if memReqs.SetUpMobilePhone == "MC Mobile Phone" else " ",
        'PersonalMobile': "x" if memReqs.SetUpMobilePhone == "Personal Mobile Phone" else " ",
        'TakePhoto': "x" if memReqs.PhotoAndBio == True else " ",
        'LibraryRouting': "x" if memReqs.LibraryRouting == True else " ",
        'QuickLawPassword': "x" if memReqs.QuicklawPassword == True else " ",
        'CreateElite': "x" if memReqs.EliteNumber == True else " ",
    }
    html_content = render_to_string('Persons/emailtemplate.html', context)
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives("New User Request", text_content, "sean.beauchamp@mcinnescooper.com", ["sean.beauchamp@mcinnescooper.com"])
    msg.attach_alternative(html_content, 'text/html')
    msg.send()
