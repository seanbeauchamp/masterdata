# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-09-21 18:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Records', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FlashRecord',
            fields=[
                ('DriveRecordID', models.AutoField(primary_key=True, serialize=False)),
                ('Time', models.DateTimeField()),
                ('Endpoint', models.CharField(max_length=50)),
                ('IP', models.CharField(max_length=100)),
                ('Software', models.CharField(max_length=100)),
                ('Process', models.CharField(max_length=100)),
                ('Filename', models.CharField(max_length=200)),
                ('Action', models.CharField(max_length=200)),
                ('User', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Records.User')),
            ],
        ),
    ]
