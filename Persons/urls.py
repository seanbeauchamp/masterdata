from django.conf.urls import url, include
from rest_framework import routers
from Persons import views

from .forms import NewHireForm, NewHireGroupsForm
from .views import NewHireWizard, FORMS, isCoreGroupNeeded
from .preview import NewHireFormPreview


router = routers.DefaultRouter()
router.register(r'persons', views.PersonSerialViewSet)
router.register(r'users', views.UserSerialViewSet)
router.register(r'groups', views.GroupSerialViewSet)

urlpatterns = [
    url(r'^$', views.main, name='main'),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^persons/$', views.list, name='list'),
    url(r'^persons/(?P<pk>\d+)/$', views.editperson, name='editperson'),
    #url(r'^newhire/', views.newperson, name='newperson'),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^newhire/', NewHireWizard.as_view(FORMS, condition_dict={"groups":isCoreGroupNeeded}), name='newperson'),
    #url(r'^newhire/$', NewHireFormPreview(NewHireForm), name='newperson'),
    #keep this functionality available, but add it *after* working out your form wizard
]