from __future__ import unicode_literals

from django.db import models

# Create your models here.
class User(models.Model):
    UserID = models.AutoField(primary_key=True)
    AccountName = models.CharField(max_length=200)
    Email = models.CharField(max_length=200)

class FlashRecord(models.Model):
    DriveRecordID = models.AutoField(primary_key=True)
    Time = models.DateTimeField()
    Endpoint = models.CharField(max_length=50)
    IP = models.CharField(max_length=100)
    User = models.ForeignKey('User')
    Software = models.CharField(max_length=100)
    Process = models.CharField(max_length=100)
    Filename = models.CharField(max_length=200)
    Action = models.CharField(max_length=200)

