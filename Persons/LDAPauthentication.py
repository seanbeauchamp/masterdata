import ldap
from django.conf import settings
from django.shortcuts import redirect


#easier than redeclaring the split string every time
def getcurrentuser(request):
    return request.user.username.split("\\")[1]


def validateuser(request):
    try:
        # currently not filtering for group. We'll add this later
        currentUser = getcurrentuser(request)
        ssoGroup = settings.LDAP['mc_law']['ALLOWED_SSO_GROUP']
        #(memberof=' + ssoGroup + ') <-- credential to add when filtering by group
        search_filter = '(&(objectClass=user)(sAMAccountName=' + currentUser + '))'
        search_attribute = ["cn", "dn", "mail", "givenName", "sn", "mailNickname"]

        con = ldap.initialize(settings.LDAP['mc_law']['SERVER'])
        con.set_option(ldap.OPT_REFERRALS, 0)
        con.simple_bind_s(settings.LDAP['mc_law']['AUTH_BIND_DN'],
                      settings.LDAP['mc_law']['AUTH_BIND_PASSWORD'])
        result = con.search_s(settings.LDAP['mc_law']['BASE'], ldap.SCOPE_SUBTREE,
                          search_filter, search_attribute)
        con.unbind()
        attributes = {}
        for attr in result[0][1]:
            try:
                attributes[attr] = result[0][1][attr][0]
                # if you can't return sAMAccountName as an attribute, you'll need a more reliable way
                # to make sure the desired results are returned, but this should do for testing;
                if currentUser == attributes['mailNickname']:
                    request.user.is_staff = True
                    return True
            except TypeError:
                return False
        return False
    except:
        return False