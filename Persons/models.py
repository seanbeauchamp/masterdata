from __future__ import unicode_literals

from django.db import models
from django import forms
import datetime

# Create your models here.
class Gender(models.Model):
    GenderID = models.AutoField(primary_key=True)
    GenderName = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.GenderName


class Company(models.Model):
    CompanyID = models.AutoField(primary_key=True)
    CompanyName = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.CompanyName


class Copier(models.Model):
    CopierID = models.AutoField(primary_key=True)
    CopierName = models.CharField(max_length=100)
    CopierLocation = models.ForeignKey('OfficeLocation')
    CopierFloor = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.CopierName


class CopierPerson(models.Model):
    CopierPersonID = models.AutoField(primary_key=True)
    CopierID = models.ForeignKey('Copier')
    PersonID = models.ForeignKey('Person')


class Department(models.Model):
    DeptID = models.AutoField(primary_key=True)
    Department = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.Department


class Group(models.Model):
    GroupID = models.AutoField(primary_key=True)
    GroupName = models.CharField(max_length=250)
    GroupEmail = models.CharField(max_length=250, blank=True, null=True)
    GroupType = models.CharField(max_length=250)
    GroupOU = models.CharField(max_length=250)

    def __str__(self):
        return self.GroupName


class JobTitle(models.Model):
    JobTitleID = models.AutoField(primary_key=True)
    JobTitle = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.JobTitle


class EmpStatus(models.Model):
    EmpStatusID = models.AutoField(primary_key=True)
    EmpStatus = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.EmpStatus


class OfficeLocation(models.Model):
    OfficeLocationID = models.AutoField(primary_key=True)
    OfficeLocation = models.CharField(max_length=250)
    RegionCode = models.CharField(max_length=3)

    def __str__(self):
        return self.OfficeLocation


class Manager(models.Model):
    ManagerID = models.AutoField(primary_key=True)
    ManagerName = models.CharField(max_length=250)
    ManagerLocation = models.ForeignKey('OfficeLocation')

    def __str__(self):
        return self.ManagerName


class MCMember(models.Model):
    MCMemberID = models.AutoField(primary_key=True)
    FirstName = models.CharField(max_length=200)
    LastName = models.CharField(max_length=200)
    AccountName = models.CharField(max_length=200)
    Email = models.CharField(max_length=250)
    Location = models.ForeignKey('OfficeLocation')

    def __str__(self):
        return self.AccountName


class TitleRole(models.Model):
    TitleID = models.ForeignKey('JobTitle')
    GroupID = models.ForeignKey('Group')
    LocationID = models.ForeignKey('OfficeLocation')

    def __str__(self):
        return str(self.TitleID) + " - " + str(self.LocationID) + " - " + str(self.GroupID)


class Person(models.Model):
    PID = models.AutoField(primary_key=True)
    FirstName = models.CharField(max_length=250, blank=True, null=True)
    MiddleName = models.CharField(max_length=250, blank=True, null=True)
    LastName = models.CharField(max_length=250, blank=True, null=True)
    Credentials = models.CharField(max_length=50, blank=True, null=True)
    MemberNum = models.IntegerField(blank=True, null=True)
    KnownBy = models.CharField(max_length=250, blank=True, null=True)
    DisplayName = models.CharField(max_length=250, blank = True, null=True) #instead of a default, use scripting to dynamically fill based one FirstName & LastName
    CompanyID = models.ForeignKey('Company', null=False, default=1)
    Title = models.CharField(max_length=250, blank=True, null=True) #Used for custom titles that don't fall under list
    JobTitleID = models.ForeignKey('JobTitle', blank=True, null=True) #a title will be required, but we'll apply in form
    CoreGroup = models.ForeignKey('Group', blank=True, null=True)
    EmpID = models.IntegerField(blank=True, null=True)
    EmpStatus = models.ForeignKey('EmpStatus', null=False)
    EmpTypeChoices = (
        ('Perm', 'Perm'),
        ('Term', 'Term')
    )
    EmpType = models.CharField(choices=EmpTypeChoices, max_length=20, null=True, blank=True)
    ManagerID = models.ForeignKey('Manager', null=True, blank=True)
    DeptID = models.ForeignKey('Department', blank=True, null=True)
    OfficeFloorID = models.IntegerField(blank=True, null=True)
    OfficeLocation = models.ForeignKey('OfficeLocation', blank=True, null=True)
    OfficeNumber = models.CharField(max_length=250, blank=True, null=True)
    BirthDate = models.DateField(blank=True, null=True)
    StartDate = models.DateField(blank=True, null=True)
    AccountName = models.CharField(max_length=20, null=False)
    TimeKeeperID = models.CharField(max_length=30, blank=True, null=True)
    EmergencyContact = models.CharField(max_length=2000, blank=True, null=True)
    IsFireMarshall = models.BooleanField(default=False)
    IsFirstAid = models.BooleanField(default=False)
    Allergy = models.CharField(max_length=2000, blank=True, null=True)
    IsActive = models.BooleanField(default=True)
    GenderID = models.ForeignKey('Gender', blank=True, null=True)
    NRR = models.ForeignKey('NRR', blank=True, null=True)
    MemberRequirements = models.ForeignKey('MemberRequirements', blank=True, null=True)

    def __str__(self):
        if self.DisplayName:
            return self.DisplayName
        else:
            return str(self.PID)


class MemberRequirements(models.Model):
    MRID = models.AutoField(primary_key=True)
    SecurityPass = models.BooleanField(default=False)
    ParkingPass = models.BooleanField(default=False)
    NameOnDoor = models.BooleanField(default=False)
    ErgonomicEvaluation = models.BooleanField(default=False)
    StandardSupplies = models.BooleanField(default=False)
    BusinessCards = models.BooleanField(default=False)
    Computer_Choices = (
        ('Laptop', 'Laptop'),
        ('Desktop', 'Desktop'),
        ('None', 'None')
    )
    SetUpComputer = models.CharField(choices=Computer_Choices, max_length=100)
    TrainingHeadset = models.BooleanField(default=False)
    PolycomPhone = models.BooleanField(default=False)
    DeskPhoneNumber = models.BooleanField(default=False)
    TimekeeperNumber = models.BooleanField(default=False)
    EliteAccess = models.BooleanField(default=False)
    OtherSoftware = models.CharField(max_length=500, blank=True, null=True)
    RemoteAccess = models.BooleanField(default=False)
    Mobile_Choices = (
        ('MC Mobile Phone', 'MC Mobile Phone'),
        ('Personal Mobile Phone', 'Personal Mobile Phone'),
        ('None', 'None')
    )
    SetUpMobilePhone = models.CharField(choices=Mobile_Choices, max_length=100)
    PhotoAndBio = models.BooleanField(default=False)
    LibraryRouting = models.BooleanField(default=False)
    QuicklawPassword = models.BooleanField(default=False)
    EliteNumber = models.BooleanField(default=False)



class NRR(models.Model):
    NRRID = models.AutoField(primary_key=True)
    NRRlabel = models.CharField(max_length=50)

    def __str__(self):
        return self.NRRlabel


class URI(models.Model):
    URID = models.AutoField(primary_key=True)
    PID = models.ForeignKey('Person', null=False)
    URIType = models.CharField(max_length=250, blank=True, null=True)
    URIAddress = models.CharField(max_length=2000, blank=True, null=True)



class AssignedAsset(models.Model):
    AssetNumber = models.AutoField(primary_key=True)
    PID = models.ForeignKey('Person')
    IsPrimary = models.BooleanField(default=False)
    DeliveryDate = models.DateTimeField(blank=True, null=True)


class Email(models.Model):
    EmailID = models.AutoField(primary_key=True)
    PID = models.ForeignKey('Person')
    EmailAddress = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.EmailAddress


class Language(models.Model):
    LanguageID = models.AutoField(primary_key=True)
    LanguageName = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.LanguageName


class PersonLanguage(models.Model):
    PID = models.ForeignKey('Person')
    LanguageID = models.ForeignKey('Language')
    Written = models.CharField(max_length=10, blank=True, null=True)
    Spoken = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        unique_together = ("PID", "LanguageID")



class PhoneProvider(models.Model):
    PhoneProviderID = models.AutoField(primary_key=True)
    Provider = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.Provider


class Country(models.Model):
    CountryID = models.AutoField(primary_key=True, max_length=4)
    CountryName = models.CharField(max_length=250, blank=True, null=True)
    CountryDialingZone = models.CharField(max_length=10, blank=True, null=True)
    Capital = models.CharField(max_length=250, blank=True, null=True)
    Currency = models.CharField(max_length=5, blank=True, null=True)
    IANA = models.CharField(max_length=5, blank=True, null=True)

    def __str__(self):
        return self.CountryName


class Province(models.Model):
    ProvinceID = models.AutoField(primary_key=True, max_length=4)
    ProvinceName = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return self.ProvinceName


class Address(models.Model):
    AddressID = models.AutoField(primary_key=True)
    AddressType = models.CharField(max_length=250, default='Business', blank=True, null=True)
    Address1 = models.CharField(max_length=2000, blank=True, null=True)
    Address2 = models.CharField(max_length=2000, blank = True, null=True)
    City = models.CharField(max_length=250, blank=True, null=True)
    ProvID = models.ForeignKey('Province', blank=True, null=True)
    CountryID = models.ForeignKey('Country', blank=True, null=True)


class Phone(models.Model):
    PhoneID = models.AutoField(primary_key=True)

