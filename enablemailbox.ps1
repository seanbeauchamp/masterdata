#Param(
#  [string]$newuser,
#  [string]$currentdb
#)
#we'll worry about parameters later. First make sure the concept works via Python

#modify this line to fit the actual credential name/location.
$admin = Get-Content testcredentials.txt
$secpassword = ConvertTo-SecureString "v3nd3tt@" -AsPlainText -Force
$usercredentials = New-Object System.Management.Automation.PSCredential("mc\smb", $secpassword)

$session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://ex15.mc.law/PowerShell/ -Authentication Kerberos -Credential $usercredentials

Import-PSSession $session

#Enable-Mailbox -Identify $newuser -Database $currentdb
Set-Mailbox -Identity "test.user" -AccountDisabled $true

Remove-PSSession $session