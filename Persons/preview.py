from django.shortcuts import redirect
from formtools.preview import FormPreview
from .models import Person, TitleRole, Group
from .views import getCoreGroup

#NOTE: Currently has some functionality for working out core groups that may be better suited for the
#form wizard. Leaving for reference sake just in case, my remove later

#covers the legwork between the newperson and preview templates, but as a result
#the actual process of saving the retrieved data needs to be called here
class NewHireFormPreview(FormPreview):
    form_template = "Persons/newperson.html"
    preview_template = "Persons/preview.html"

    def done(self, request, cleaned_data):
        #To ADD: Actual saving functionality
        return redirect("list")

    def process_preview(self, request, form, context):
        preview_data = {}
        for key, value in form.cleaned_data.iteritems():
            preview_data[key] = value

        #try giving an additional option for choosing a core group if the user chose a custom title
        if preview_data['Title'] != None:
            roleOptions = getRegionalCoreGroups(form.cleaned_data["OfficeLocation"])
            context['coreForm'] = roleOptions
        else:
            coreGroupData = getCoreGroup(request, form.cleaned_data["OfficeLocation"], form.cleaned_data["JobTitleID"])
            preview_data['Core Group'] = coreGroupData
        context['preview_data'] = preview_data


#results can be used to limit core group options to only those applicable for the new user's location
def getRegionalCoreGroups(officeLocation):
    applicableRoles = TitleRole.objects.filter(LocationID=officeLocation).values("GroupID").distinct()
    applicableGroups = []
    for role in applicableRoles:
        currentGroup = Group.objects.get(GroupID=role["GroupID"])
        applicableGroups.append(currentGroup)
    return applicableGroups